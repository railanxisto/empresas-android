![N|Solid](logo_ioasys.png)

# README #

### GERAL ###

Projeto desenvolvido em Kotlin  na arquitetura  MVVM e seguindo algumas das recomendações do Google Android Jetpack, entre elas:

* ViewModel - arch component
* DataBinding
* LiveData

Com mais tempo seriam adicionados:

- Testes Unitários
- Melhorar a exibição de imagens quando carregando ou quando a empresa não possui imagem;
- Salvar os dados para o Header no Room e evitar Singletons. 
- Fazer cache local(Dependia de mudanças de requisito de acordo com o design do Zeplin)
- Melhorar a generalização dos resultados da API

### DEPENDÊNCIAS ADICIONADAS AO PROJETO ###

* Coroutines
	Para execução assíncrona
* Koin
	Para injecão de dependência
* Glide 
	Para cache e loading de imagens	
* Retrofit
	Para realizar requisições
* Gson
	Para parse de Json
* Okttp Logging Interceptor 
	Para interceptar requisições e responses

### DADOS PARA TESTE ###

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234
