package com.railanxisto.empresas.data.remote.repositories

import com.railanxisto.empresas.data.local.UserSingleton
import com.railanxisto.empresas.data.remote.ApiService
import com.railanxisto.empresas.data.remote.config.ACCESS_TOKEN
import com.railanxisto.empresas.data.remote.config.CLIENT
import com.railanxisto.empresas.data.remote.config.DefaultInterceptor
import com.railanxisto.empresas.data.remote.config.UID
import com.railanxisto.empresas.data.remote.model.LoginResponse
import com.railanxisto.empresas.data.remote.utils.ApiResult
import com.railanxisto.empresas.data.remote.utils.result
import com.railanxisto.empresas.data.remote.utils.safeCall

interface LoginRepository {
    suspend fun login(email: String, password: String): ApiResult<LoginResponse>
}

class LoginRepositoryImpl(val apiService: ApiService) : LoginRepository {
    override suspend fun login(email: String, password: String): ApiResult<LoginResponse> {
        val response =  safeCall { apiService.login(email, password) }
        UserSingleton.accesstoken = response.headers().get(ACCESS_TOKEN)
        UserSingleton.uid = response.headers().get(UID)
        UserSingleton.client = response.headers().get(CLIENT)
        return response.result()
    }
}