package com.railanxisto.empresas.data.remote

import com.railanxisto.empresas.data.remote.config.RetrofitManager
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.data.remote.model.EnterpriseResponse
import com.railanxisto.empresas.data.remote.model.LoginResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    companion object {
        fun create(): ApiService {
            val retrofit = RetrofitManager()
            return retrofit.retrofitInstance().create(ApiService::class.java)
        }
    }

    /**
     * Login
     */
    @FormUrlEncoded
    @POST("users/auth/sign_in")
    fun login(@Field("email") email: String, @Field("password") password: String): Call<LoginResponse>

    /**
     * Enterprises
     */
    @GET("enterprises")
    fun enterprises(@Query(ApiConstants.QUERY_NAME) name: String): Call<EnterpriseResponse>

}