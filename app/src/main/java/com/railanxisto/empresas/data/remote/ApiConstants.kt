package com.railanxisto.empresas.data.remote

class ApiConstants {
    companion object {
        const val URL_BASE = "http://empresas.ioasys.com.br/api/"
        const val TIMEOUT_CONNECTION: Long = 4
        const val READ_WRITE_TIMEOUT: Long = 60
        const val API_VERSION = "v1/"
        const val QUERY_NAME = "name"
    }
}