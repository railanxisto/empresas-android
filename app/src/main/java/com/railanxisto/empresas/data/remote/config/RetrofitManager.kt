package com.railanxisto.empresas.data.remote.config

import android.provider.SyncStateContract
import com.railanxisto.empresas.data.remote.ApiConstants
import okhttp3.OkHttpClient
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitManager : KoinComponent {

    val httpClient: OkHttpClient by inject()

    fun retrofitInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ApiConstants.URL_BASE + ApiConstants.API_VERSION)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()
    }
}