package com.railanxisto.empresas.data.local

object UserSingleton {
    var accesstoken: String? = null
    var uid: String? = null
    var client: String? = null
}