package com.railanxisto.empresas.data.remote.repositories

import com.railanxisto.empresas.data.remote.ApiService
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.data.remote.model.EnterpriseResponse
import com.railanxisto.empresas.data.remote.model.LoginResponse
import com.railanxisto.empresas.data.remote.utils.ApiResult
import com.railanxisto.empresas.data.remote.utils.result
import com.railanxisto.empresas.data.remote.utils.safeCall

interface EnterpriseRepository {
    suspend fun getEnterprises(name: String): ApiResult<EnterpriseResponse>
}

class EnterpriseRepositoryImpl(val apiService: ApiService) : EnterpriseRepository {
    override suspend fun getEnterprises(name: String): ApiResult<EnterpriseResponse> {
        val response =  safeCall { apiService.enterprises(name = name) }
        return response.result()
    }
}