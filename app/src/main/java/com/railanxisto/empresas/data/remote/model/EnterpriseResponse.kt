package com.railanxisto.empresas.data.remote.model

data class EnterpriseResponse(
    val enterprises: List<Enterprise>
)