package com.railanxisto.empresas.data.remote.model

import java.io.Serializable

data class Enterprise (
    var enterprise_name: String? = "",
    var photo: String? = "",
    var description: String? = "",
    var country: String? = "",
    var enterprise_type: TypeEnterprise
) : Serializable