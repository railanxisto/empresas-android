package com.railanxisto.empresas.data.remote.utils

import com.railanxisto.empresas.data.remote.config.RetrofitManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Response
import java.lang.Error

sealed class ApiResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : ApiResult<T>()
    data class Error(val erro: String) : ApiResult<Nothing>()
}

fun <T : Any> Response<T>.result(): ApiResult<T> {
    if (this.isSuccessful) {
        return ApiResult.Success(this.body()!!)
    }
    return try {
        val converter: Converter<ResponseBody, ApiResult.Error> = RetrofitManager().retrofitInstance().responseBodyConverter(Error::class.java, arrayOfNulls<Annotation>(0))
        val converted = converter.convert(errorBody())
        ApiResult.Error(converted!!.erro)
    } catch (exception: Exception) {
        ApiResult.Error("Erro Interno. Tente Novamente")
    }
}

suspend fun <T : Any> safeCall(call: suspend () -> Call<T>): Response<T> = withContext(Dispatchers.Main) {
    withContext(Dispatchers.IO) {
        try {
            call.invoke().execute()
        } catch (exception: Exception) {
            if (exception.message == "Canceled") {
                exception.printStackTrace()
                Response.error<T>(700, ResponseBody.create(MediaType.parse("application/json"), "{}"))
            } else {
                exception.printStackTrace()
                Response.error<T>(500, ResponseBody.create(MediaType.parse("application/json"), "{}"))
            }
        }
    }
}
