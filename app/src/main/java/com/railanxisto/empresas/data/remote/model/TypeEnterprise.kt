package com.railanxisto.empresas.data.remote.model

import java.io.Serializable

data class TypeEnterprise (
    val id: Int?,
    val enterprise_type_name: String? = ""
): Serializable
