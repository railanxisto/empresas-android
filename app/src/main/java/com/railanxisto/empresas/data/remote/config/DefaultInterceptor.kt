package com.railanxisto.empresas.data.remote.config

import com.railanxisto.empresas.data.local.UserSingleton
import okhttp3.Interceptor
import okhttp3.Response

const val ACCESS_TOKEN = "access-token"
const val UID = "uid"
const val CLIENT = "client"

class DefaultInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response? {
        val original = chain.request()
        val requestBuilder = original.newBuilder()
        if (UserSingleton.accesstoken != null) {
            requestBuilder.header(ACCESS_TOKEN, UserSingleton.accesstoken!!)
        }
        if (UserSingleton.uid != null) {
            requestBuilder.header(UID, UserSingleton.uid!!)
        }
        if (UserSingleton.client != null) {
            requestBuilder.header(CLIENT, UserSingleton.client!!)
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}