package com.railanxisto.empresas.di

import com.railanxisto.empresas.data.remote.repositories.EnterpriseRepository
import com.railanxisto.empresas.data.remote.repositories.EnterpriseRepositoryImpl
import com.railanxisto.empresas.data.remote.repositories.LoginRepository
import com.railanxisto.empresas.data.remote.repositories.LoginRepositoryImpl
import org.koin.dsl.module.module

val repositoryModule = module {

    // Login repository
    single<LoginRepository> { LoginRepositoryImpl(get()) }

    // Enterprise Repository
    single<EnterpriseRepository> { EnterpriseRepositoryImpl(get()) }
}