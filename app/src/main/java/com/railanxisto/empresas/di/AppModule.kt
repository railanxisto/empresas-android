package com.railanxisto.empresas.di

import com.railanxisto.empresas.ui.enterprises.EnterprisesViewModel
import com.railanxisto.empresas.ui.login.LoginViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {

    // Login ViewModel
    viewModel { LoginViewModel(get()) }

    // Enterprises ViewModel
    viewModel { EnterprisesViewModel(get()) }
}