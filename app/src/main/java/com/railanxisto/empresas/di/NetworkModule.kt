package com.railanxisto.empresas.di

import com.railanxisto.empresas.data.remote.ApiService
import com.railanxisto.empresas.data.remote.config.DefaultInterceptor
import com.railanxisto.empresas.data.remote.config.HttpClientImpl
import com.railanxisto.empresas.data.remote.config.RetrofitManager
import org.koin.dsl.module.module

val networkModule = module {
    factory { DefaultInterceptor() }

    single { HttpClientImpl( get()).create() }

    single { RetrofitManager() }
    single { ApiService.create() }
}