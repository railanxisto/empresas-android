package com.railanxisto.empresas

import android.app.Application
import com.railanxisto.empresas.di.appModule
import com.railanxisto.empresas.di.networkModule
import com.railanxisto.empresas.di.repositoryModule
import org.koin.android.ext.android.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule, networkModule, repositoryModule))
    }
}