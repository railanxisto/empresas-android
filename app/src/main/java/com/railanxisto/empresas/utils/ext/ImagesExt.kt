package com.railanxisto.empresas.utils.ext

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.railanxisto.empresas.utils.Constants

fun ImageView.downloadImage(photo: String) {
    Glide.with(context)
        .load(Constants.URL_BASE_IMAGES + photo)
        // .placeholder()
        .into(this)
}