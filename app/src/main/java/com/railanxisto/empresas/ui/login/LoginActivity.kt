package com.railanxisto.empresas.ui.login

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.railanxisto.empresas.R
import com.railanxisto.empresas.databinding.ActivityLoginBinding
import org.koin.android.ext.android.inject
import android.databinding.DataBindingUtil
import android.view.View
import android.widget.Toast
import android.content.Intent
import com.railanxisto.empresas.ui.enterprises.EnterprisesActivity


class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    val viewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login) as ActivityLoginBinding

        binding.loginButton.setOnClickListener {
            viewModel.doLogin(binding.emailEditText.text.toString(), binding.passwordEditText.text.toString())
        }

        createObservers()
    }

    private fun createObservers() {
        viewModel.getLoginResponse().observe(this, Observer {
            if (it!!) {
                val intent = Intent(this@LoginActivity, EnterprisesActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(this, "Falha no Login. Verifique as informações e informe os dados corretamente.", Toast.LENGTH_LONG).show()
            }
        })

        viewModel.getLoading().observe(this, Observer {
            if (it!!) {
                binding.progressBar.visibility = View.VISIBLE
                binding.loginButton.visibility = View.GONE
            } else {
                binding.progressBar.visibility = View.GONE
                binding.loginButton.visibility = View.VISIBLE
            }
        })
    }
}
