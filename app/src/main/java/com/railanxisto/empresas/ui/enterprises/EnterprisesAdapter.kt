package com.railanxisto.empresas.ui.enterprises

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.databinding.EnterpriseItemBinding
import com.railanxisto.empresas.R
import com.railanxisto.empresas.utils.ext.downloadImage

class EnterprisesAdapter(val context: Context, private var enterprises: List<Enterprise>, private val listener: OnItemAdapterClickListener) : RecyclerView.Adapter<EnterprisesAdapter.ViewHolder>() {

    fun updateEnterprises(enterprises: List<Enterprise>) {
        this.enterprises = enterprises
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return enterprises.size
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // TODO: Temporary recyclable false.
        holder.setIsRecyclable(false)

        val enterprise = enterprises[position]
        holder.binding.enterpriseRowLayout.setOnClickListener {
            listener.onItemClick(enterprise)
        }
        if (enterprise.photo != null) {
            holder.binding.imagemEmpresaImageView.downloadImage(enterprise.photo!!)
        } else {
            holder.binding.imagemEmpresaImageView.background = ContextCompat.getDrawable(context, R.color.no_picture_enterprise)
        }
        holder.bind(enterprise)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: EnterpriseItemBinding = EnterpriseItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: EnterpriseItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(enterprise: Enterprise) {
            binding.enterprise = enterprise
            binding.executePendingBindings()
        }
    }

    interface OnItemAdapterClickListener {
        fun onItemClick(enterprise: Enterprise)
    }
}