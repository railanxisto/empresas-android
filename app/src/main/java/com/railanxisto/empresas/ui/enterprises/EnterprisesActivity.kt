package com.railanxisto.empresas.ui.enterprises

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.railanxisto.empresas.R
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.SearchView
import android.view.Menu
import com.railanxisto.empresas.databinding.ActivityMainBinding
import android.view.View
import android.widget.EditText
import android.widget.Toast
import org.koin.android.ext.android.inject
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.ui.detailsEnterprise.DetailsEnterpriseActivity

class EnterprisesActivity : AppCompatActivity(), EnterprisesAdapter.OnItemAdapterClickListener  {

    override fun onItemClick(enterprise: Enterprise) {
        val intent = Intent(this@EnterprisesActivity, DetailsEnterpriseActivity::class.java)
        intent.putExtra("enterprise", enterprise)
        startActivity(intent)
    }

    lateinit var binding: ActivityMainBinding
    val viewModel: EnterprisesViewModel by inject()
    private lateinit var enterprisesAdapter: EnterprisesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main) as ActivityMainBinding
        setSupportActionBar(binding.toolbar)
        createObservers()
        setRecyclerView()
    }

    private fun createObservers() {
        viewModel.getEnterprisesResponse().observe(this, Observer {
            if (it!!.size > 0) {
                enterprisesAdapter.updateEnterprises(it)
                binding.enterpriseRecyclerView.visibility = View.VISIBLE
                binding.noResultsTextView.visibility = View.GONE
            } else {
                binding.enterpriseRecyclerView.visibility = View.GONE
                binding.noResultsTextView.visibility = View.VISIBLE

            }
        })

        viewModel.getLoading().observe(this, Observer {
            if (it!!) {
                binding.dataLayout.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
                binding.dataLayout.visibility = View.VISIBLE
            }
        })
    }

    private fun setRecyclerView() {
        enterprisesAdapter = EnterprisesAdapter(this, emptyList(), this)
        binding.enterpriseRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.enterpriseRecyclerView.setHasFixedSize(true)
        binding.enterpriseRecyclerView.adapter = enterprisesAdapter
    }

    private fun search(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                viewModel.getEnterprises(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu.findItem(R.id.action_search)
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = getString(R.string.search)
        val searchEditText = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        searchEditText.setTextColor(ContextCompat.getColor(this, R.color.white))
        searchEditText.setHintTextColor(ContextCompat.getColor(this, R.color.search_hint))

        menuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                binding.titleTextView.visibility = View.GONE
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                binding.titleTextView.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
                binding.enterpriseRecyclerView.visibility = View.GONE
                binding.noResultsTextView.visibility = View.GONE
                return true
            }
        })

        search(searchView)
        return super.onCreateOptionsMenu(menu)
    }

}
