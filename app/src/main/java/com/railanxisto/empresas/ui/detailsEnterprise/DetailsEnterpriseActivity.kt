package com.railanxisto.empresas.ui.detailsEnterprise

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.railanxisto.empresas.R
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.databinding.ActivityDetailsEnterpriseBinding
import com.railanxisto.empresas.utils.ext.downloadImage

class DetailsEnterpriseActivity : AppCompatActivity() {

    lateinit var enterprise: Enterprise
    lateinit var binding: ActivityDetailsEnterpriseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details_enterprise) as ActivityDetailsEnterpriseBinding

        if (intent.extras != null) {
            enterprise = intent.extras.get("enterprise") as Enterprise
        }

        binding.toolbar.title = enterprise.enterprise_name
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        binding.descriptionEnterpriseTextView.text = enterprise.description
        if (enterprise.photo != null) {
            binding.imagemEnterpriseImageView.downloadImage(enterprise.photo!!)
        } else {
            binding.imagemEnterpriseImageView.setBackgroundColor(ContextCompat.getColor(this, R.color.no_picture_enterprise))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
