package com.railanxisto.empresas.ui.enterprises

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.railanxisto.empresas.data.remote.model.Enterprise
import com.railanxisto.empresas.data.remote.repositories.EnterpriseRepository
import com.railanxisto.empresas.data.remote.utils.ApiResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class EnterprisesViewModel(private val repository: EnterpriseRepository) : ViewModel(), CoroutineScope {

    private val job = SupervisorJob()

    override val coroutineContext: kotlin.coroutines.CoroutineContext
        get() = Dispatchers.Main + job

    private val enterprisesList = MutableLiveData<List<Enterprise>>()
    private val loading = MutableLiveData<Boolean>()
    private val erroLista = MutableLiveData<Boolean>()

    fun getEnterprises(name: String) {
        launch {
            loading.value = true
            val response = repository.getEnterprises(name)
            when (response) {
                is ApiResult.Success -> {
                    enterprisesList.value = response.data.enterprises
                }
                is ApiResult.Error -> {
                    erroLista.value = true
                }
            }
            loading.value = false
        }
    }

    fun getEnterprisesResponse(): LiveData<List<Enterprise>> {
        return enterprisesList
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }
}