package com.railanxisto.empresas.ui.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.railanxisto.empresas.data.local.UserSingleton
import com.railanxisto.empresas.data.remote.config.ACCESS_TOKEN
import com.railanxisto.empresas.data.remote.model.LoginResponse
import com.railanxisto.empresas.data.remote.repositories.LoginRepository
import com.railanxisto.empresas.data.remote.utils.ApiResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: LoginRepository) : ViewModel(), CoroutineScope {

    private val job = SupervisorJob()

    override val coroutineContext: kotlin.coroutines.CoroutineContext
        get() = Dispatchers.Main + job

    private val loginSucess = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()

    fun doLogin(email: String, password: String) {
        launch {
            loading.value = true
            val loginResponse = repository.login(email, password)
            when (loginResponse) {
                is ApiResult.Success ->
                {
                    loginSucess.value = loginResponse.data.success
                }
                is ApiResult.Error -> {
                    loginSucess.value = false
                }
            }
            loading.value = false
        }
    }

    fun getLoginResponse(): LiveData<Boolean> {
        return loginSucess
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }
}